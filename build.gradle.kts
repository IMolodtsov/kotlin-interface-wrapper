plugins {
    java
    kotlin("jvm") version "1.3.61"
}

group = "org.example"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    testCompile("junit", "junit", "4.12")
    compile(kotlin("script-runtime"))
    compile(kotlin("script-util"))
    compile(kotlin("compiler-embeddable"))
    compile(kotlin("scripting-compiler-embeddable"))
    compile("org.jetbrains.kotlin","kotlin-reflect", "1.3.70")
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_1_8
}

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
}