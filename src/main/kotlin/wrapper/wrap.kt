package wrapper

import javax.script.ScriptEngineManager
import kotlin.reflect.KClass
import kotlin.reflect.KType
import kotlin.reflect.full.*


fun<T:Any> wrap(clazz: KClass<T>): Constructor<T> {
    val cl = clazz.java.classLoader
    val name = clazz.simpleName
    val pack = clazz.java.packageName
    var properties = ""
    for (p in clazz.memberProperties) {
        properties += wrap(p.name, p.returnType)
    }
    val script = """
        import $pack.$name
        import wrapper.Constructor
        
        class ${name}Wrapper(private val obj: HashMap<String, Any>): $name {
            $properties
        }
        
        object : Constructor<$name> {
            override fun invoke(vararg args: Any): $name {
                return ${name}Wrapper(args[0] as HashMap<String, Any>)
            }
        }
    """.trimIndent()

    val engine = ScriptEngineManager(cl).getEngineByExtension("kts")!!
    return engine.eval(script) as Constructor<T>
}

fun wrap(name: String, type: KType ): String {
    return """
        
        override var $name: $type
            get() = obj["$name"] as $type
            set(value) {
                obj["$name"] = value as Any
            }
    """.trimIndent()
}