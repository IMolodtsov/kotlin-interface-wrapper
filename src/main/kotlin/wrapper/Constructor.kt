package wrapper

interface Constructor<T>: (Array<out Any>)->T {
    override operator fun invoke(vararg args: Any): T
}