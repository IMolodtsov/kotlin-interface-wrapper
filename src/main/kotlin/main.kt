import org.jetbrains.kotlin.cli.common.environment.setIdeaIoUseFallback
import pack.A
import wrapper.wrap

fun main(args: Array<String>) {
    setIdeaIoUseFallback()
    val obj = HashMap<String, Any>()
    obj["P1"] = 1
    obj["P2"] = "1a"
    obj["P3"] = arrayOf(3)

    val constructor = wrap(A::class)
    val inst = constructor(obj)

    println(inst.P1)
    println(inst.P2)
    println(inst.P3[0])
}